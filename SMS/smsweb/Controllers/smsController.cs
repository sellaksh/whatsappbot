﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Twilio.TwiML;
using Twilio.Types;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.AspNet.Mvc;
using Twilio.AspNet.Common;
using System.Threading.Tasks;

namespace smsweb.Controllers
{
    public class smsController : TwilioController
    {
        public async Task<TwiMLResult> IndexAsync(SmsRequest incomingMessage)
        {
            MessagingResponse messagingResponse = new MessagingResponse();
            Chat msg;
            var dlc = new DirectLineController();
            Chat c = new Chat();
            c.ChatMessage = incomingMessage.Body;
            dlc.ControllerContext = ControllerContext;
           
            if(incomingMessage.Body == null)
            {
                await dlc.Index();
            }
            else
            {
               msg = await dlc.Index(c);
                messagingResponse.Message(
                                     msg.ChatResponse);
            }
            return TwiML(messagingResponse);
        }
   
    }
}