﻿using Microsoft.Bot.Connector.DirectLine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace smsweb.Controllers
{
    public class MyDirectLineIntegration
    {
        private readonly DirectLineClient _directLineClient;
        private Conversation _conversation;
        private string _watermark;

        private const string From = "Kristian";

        public MyDirectLineIntegration()
        {
            const string secret = "pQvY5THluMc.LcbInbZzkLLAG76wA3tlhTxDIzk4HxYZUFIEUT3FIQ4";
            _directLineClient = new DirectLineClient(secret);
        }

        public async Task StartConversation()
        {
            _conversation = await _directLineClient.Conversations
                .StartConversationAsync().ConfigureAwait(false);
        }
        public async Task SendMessage(string message)
        {
            var fromProperty = new ChannelAccount(From);
            var activity = new Activity(text: message, fromProperty: fromProperty, type: "message");
            await _directLineClient.Conversations
                .PostActivityAsync(_conversation.ConversationId, activity).ConfigureAwait(false);
        }

        public async Task<IList<Activity>> GetMessages()
        {
            var response = await _directLineClient.Conversations
                .GetActivitiesAsync(_conversation.ConversationId, _watermark).ConfigureAwait(false);
            _watermark = response.Watermark;
            return response.Activities;
        }
    }
}