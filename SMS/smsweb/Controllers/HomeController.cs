﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Net;
using Microsoft.Bot.Connector.DirectLine;

namespace smsweb.Controllers
{
    public class Chat
    {
        public string ChatMessage { get; set; }
        public string ChatResponse { get; set; }
        public string watermark { get; set; }
    }
    public class HomeController : Controller
    {
        private static string DiretlineUrl
                = @"https://directline.botframework.com";
        private static string directLineSecret =
            "pQvY5THluMc.LcbInbZzkLLAG76wA3tlhTxDIzk4HxYZUFIEUT3FIQ4";
        private static string botId =
            "bottestFAQ12";
        public async Task<ActionResult> Index()
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls | SecurityProtocolType.Ssl3;
            // Create an Instance of the Chat object
            Chat objChat = new Chat();
            // Only call Bot if logged in
            // Return response
            objChat = await TalkToTheBot("Hello");
            return View(objChat);
        }
        private async Task<Chat> TalkToTheBot(string paramMessage)
        { 
            // Connect to the DirectLine service
            DirectLineClient client = new DirectLineClient(directLineSecret);
            // Try to get the existing Conversation
            Conversation conversation =
                System.Web.HttpContext.Current.Session["conversation"] as Conversation;
            // Try to get an existing watermark 
            // the watermark marks the last message we received
            string watermark =
                System.Web.HttpContext.Current.Session["watermark"] as string;
            if (conversation == null)
            {
                // There is no existing conversation
                // start a new one
                //var tokendl = client.Tokens.GenerateTokenForNewConversation();
                conversation = client.Conversations.StartConversation();
                //conversation = await client.Conversations.StartConversationAsync();
            }
            // Use the text passed to the method (by the user)
            // to create a new message
            Activity userMessage = new Activity
            {
                From = new ChannelAccount(User.Identity.Name),
                Text = paramMessage,
                Type = ActivityTypes.Message
            };
            // Post the message to the Bot
            await client.Conversations.PostActivityAsync(conversation.ConversationId, userMessage);
            // Get the response as a Chat object
            Chat objChat =
                await ReadBotMessagesAsync(client, conversation.ConversationId, watermark);
            // Save values
            System.Web.HttpContext.Current.Session["conversation"] = conversation;
            System.Web.HttpContext.Current.Session["watermark"] = objChat.watermark;
            // Return the response as a Chat object
            return objChat;
        }
        private async Task<Chat> ReadBotMessagesAsync(
            DirectLineClient client, string conversationId, string watermark)
        {
            // Create an Instance of the Chat object
            Chat objChat = new Chat();
            // We want to keep waiting until a message is received
            bool messageReceived = false;
            while (!messageReceived)
            {
                // Retrieve the activity set from the bot.
                var activitySet = await client.Conversations.GetActivitiesAsync(conversationId, watermark);
                // Set the watermark to the message received
                watermark = activitySet?.Watermark;
                // Extract the activies sent from our bot.
                var activities = (from Activity in activitySet.Activities
                                  where Activity.From.Id == botId
                                  select Activity).ToList();
                // Analyze each activity in the activity set.
                foreach (Activity activity in activities)
                {
                    // Set the text response
                    // to the message text
                    objChat.ChatResponse
                        += " "
                        + activity.Text.Replace("\n\n", "<br />");
                    // Are there any attachments?
                    if (activity.Attachments != null)
                    {
                        // Extract each attachment from the activity.
                        foreach (Attachment attachment in activity.Attachments)
                        {
                            switch (attachment.ContentType)
                            {
                                case "image/png":
                                    // Set the text response as an HTML link
                                    // to the image
                                    objChat.ChatResponse
                                        += " "
                                        + attachment.ContentUrl;
                                    break;
                            }
                        }
                    }
                }
                // Mark messageReceived so we can break 
                // out of the loop
                messageReceived = true;
            }
            // Set watermark on the Chat object that will be 
            // returned
            objChat.watermark = watermark;
            // Return a response as a Chat object
            return objChat;
        }
        
    }
}